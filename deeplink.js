// on startup

const url = await Linking.getInitialURL();

if (url) { this.handleDeeplink(url); }

// handleDeeplink function

	handleDeeplink = (url, isAuthenticated=true) => {
		appsFlyer.sendDeepLinkData(url);

		// movic://car-list?city=Jabodetabek&start_date=2020-05-05&end_date=2020-05-06&order=lowest-price&time=22:20&regency_id=27182b53-dbfc-4ae5-a232-667628ff2807
		// https://movic.id/mobile-app/info/55cd14a5-33c5-4ffc-85cb-3310bde85b8f?af_android_custom_url=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Did.astra.adp.movic&af_deeplink=true&af_web_id=523e1156-aabb-4426-846d-3b9540153371-o&campaign=None&media_source=Social%20Facebook&shortlink=f1495181

		// clone string
		let newUrl = `${url}`;

		newUrl = newUrl.slice(newUrl.indexOf('://') + 3);

		if(newUrl.indexOf('mobile-app') >= 0) {
			newUrl = newUrl.slice(newUrl.indexOf('mobile-app/') + 'mobile-app/'.length);
		}

		console.log(url, newUrl);

		const [rawUri, params = ""] = newUrl.split('?');
		const [uriType, uriId] = rawUri.split('/');

		const uri = {
			type: uriType || '',
			params: uriId || '',
			query: params
				.split('&')
				.reduce((all, d) => {

					const [key, value] = d.split('=');

					all[key]=value;

					return all;
				}, {})
		};


		if(!isAuthenticated) {
			if (uri.type === 'register') {
				const registerResult = this.props.navigation.navigate('Register', {
				});
				console.log(registerResult, 'registerResult');
				return;
			}
		}

		if (uri.type === 'select_location') {
			this.props.navigation.navigate('CarSearchUser', {
				searchType: 'CAR_WITH_DRIVER'
			});

			return;
		}

		if (uri.type === 'rental') {
			this.props.navigation.navigate('CarRentalDetail', {
				id: uri.params
			});

			return;
		}

		if (uri.type === 'promo') {
			this.props.navigation.navigate('PromoDetailUser', {
				id: uri.params
			});

			return;
		}

		if (uri.type === 'car') {
			this.props.store.car.selectedCar.id = uri.params;
			this.props.navigation.navigate('CarDetailUser', {
				id: uri.params,
				fromRental: true
			});

			return;
		}

		if (uri.type === 'car-airport') {
			this.props.store.airport_car.selectedCar.id = uri.params;
			this.props.navigation.navigate('CarDetailAirportUser', {
				id: uri.params, fromRental: true
			});

			return;
		}

		if (uri.type === 'siap-antar') {
			this.props.store.ui.isSiapAntarModalOpened = true;

			this.props.navigation.navigate('HomeUser', {
			});

			return;
		}

		if (uri.type === 'car-list') {

			console.log({uri}, 'StartupScreen -> handleDeeplink')

			Object.keys(uri.query || {})
				.map(k => {
					this.props.store.car.searchQuery[k] = uri.query[k];
				});

			const location = {
				id: uri.query.regency_id,
				name: uri.query.city,
			};

			this.props.store.carSearch.locationData = location;

			let startDate = uri.query.start_date;
			let endDate = uri.query.end_date;

			if(uri.query.set_date && uri.query.set_date === 'tomorrow') {

				startDate = moment().add(1, 'day').format('YYYY-MM-DD');
				endDate = moment().add(1, 'day').format('YYYY-MM-DD');
			}

			this.props.store.order.data.startDate = startDate;
			this.props.store.order.data.endDate = endDate;

			this.props.store.carSearch.start_date = startDate;
			this.props.store.carSearch.end_date = endDate;

			this.props.store.car.carSearch.start_date = startDate;
			this.props.store.car.carSearch.end_date = endDate;

			this.props.navigation.navigate('CarBookingV2User', {
				id: uri.params,
				fromRental: true,
				selectedRegency: location
			});

			return;
		} if (uri.type === 'info') {
			this.props.navigation.navigate('InfoDetailUser', {
				id: uri.params
			});

			return;
		}

		if (uri.type === 'car-airport-search') {
			this.props.navigation.navigate('AirportRentSearchUser', {
			});

			return;
		}

	}

